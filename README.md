# README #

### What is this repository for? ###

* A coding challenge for [MedCircle](https://github.com/MedCircle/medcircle-coding-project)

### Known Issues ###

* Avatar and body flicker when selecting a post
* Extra space at end of cell on different size devices
* Need a smarter way to fetch media in batches instead of all at once,  
but at the same time can't get it one by one or else it's too laggy
### Who do I talk to? ###

* Haskel Ash ([Bitbucket](https://bitbucket.org/haskelash), [GitHub](https://github.com/haskelash), [Gmail](mailto:haskelash@gmail.com))