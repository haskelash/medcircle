//
//  DataSource.swift
//  MedCircle
//
//  Created by Haskel Ash on 8/1/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import UIKit

class DataSource: NSObject, UITableViewDataSource {

    var json: NSMutableArray
    var tableView: UITableView
    var expandedRows: CFMutableBitVector

    init(json: NSMutableArray, tableView: UITableView) {
        self.json = json
        self.tableView = tableView
        expandedRows = CFBitVectorCreateMutable(kCFAllocatorDefault, json.count)
        super.init()

        tableView.dataSource = self

        //cache avatar and media images and articles so we only download them once
        cacheData()
    }

    private func cacheData() {
        //for each dict in the json
        for case let dict as NSMutableDictionary in json {
            //get the author icon url
            if let string = dict["author"]?["icon_url"] as? String,
                let url = NSURL(string: string) {
                //request the icon
                NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {
                    data, response, error in

                    if let data = data,
                        let icon = UIImage(data: data),
                        let authorDict = dict["author"] as? NSMutableDictionary {

                        //add the icon to the json and reload the cell
                        authorDict["icon"] = icon
                        self.reloadRowOnMain(self.json.indexOfObject(dict))
                    }
                }).resume()
            }

            //get the media url
            if let string = dict["media_url"] as? String,
                let url = NSURL(string: string) {
                //request the media
                NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {
                    data, response, error in

                    if let data = data,
                        let media = UIImage(data: data) {

                        //add the media to the json and reload the cell
                        dict["media"] = media
                        self.reloadRowOnMain(self.json.indexOfObject(dict))
                    }
                }).resume()
            }

            //get the article url
            if let id = dict["id"] as? Int,
                let url = NSURL(string: "https://medcircle-coding-project.s3.amazonaws.com/api/articles/\(id).json") {
                //request the article
                NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {
                    data, response, error in

                    //turn it to json
                    do {
                        if let data = data,
                            let article = try NSJSONSerialization.JSONObjectWithData(
                            data, options: .MutableContainers) as? NSMutableDictionary {
                            //add the article to the main json and reload the cell
                            dict["body"] = article["body"]
                            self.reloadRowOnMain(self.json.indexOfObject(dict))
                        }
                    } catch let error as NSError {
                        print(error.description)
                    }
                }).resume()
            }
        }
    }

    private func reloadRowOnMain(row: Int) {
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadRowsAtIndexPaths([
                NSIndexPath(forRow:row, inSection: 0)],
                withRowAnimation: .Automatic)
        })
    }

    func toggleRow(row: Int) {
        CFBitVectorFlipBitAtIndex(expandedRows, row)
    }

    func expandedHeightDiffForRow(row: Int) -> CGFloat {
        //if we have body text for the row, return it's height - 80 (80 is the compact height)

        if let body = json[row]["body"] as? NSString {

            let boundingBox = body.boundingRectWithSize(
                CGSizeMake(tableView.frame.width, CGFloat.max),
                options: .UsesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)],
                context: nil)
            return boundingBox.height - 80
        }

        return 0
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return json.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //get the cell, inject the data from the json, tell if it is expanded, and give it its expanded height diff

        let cell = tableView.dequeueReusableCellWithIdentifier("MedCell", forIndexPath: indexPath) as! MedCell
        cell.inject(dict: json[indexPath.row] as! NSDictionary,
                    expanded: CFBitVectorGetBitAtIndex(expandedRows, indexPath.row) == 1,
                    heightToExpand: expandedHeightDiffForRow(indexPath.row))
        return cell
    }
}
