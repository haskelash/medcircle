//
//  ViewController.swift
//  MedCircle
//
//  Created by Haskel Ash on 8/1/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    var dataSource: DataSource?
    let contractedCellHeight: CGFloat = 430

    override func viewDidLoad() {
        super.viewDidLoad()

        //get the json from the server
        let url = NSURL(string: "https://medcircle-coding-project.s3.amazonaws.com/api/articles.json")!
        NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {
            data, response, error in

            //turn it to json
            do {
                if let data = data,
                    json = try NSJSONSerialization.JSONObjectWithData(
                        data, options: .MutableContainers) as? NSMutableArray {

                    //create a data source and pass it the json
                    self.dataSource = DataSource(json: json, tableView: self.tableView)
                    self.tableView.delegate = self

                    //update the table view on main thread
                    dispatch_async(dispatch_get_main_queue(), {
                        self.tableView.reloadData()
                    })
                }
            } catch let error as NSError {
                print(error.description)
            }
        }).resume()
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        //if this row is expanded, and we have body text, add the amount to exand by
        //otherwise just return the contracted height

        if let dataSource = dataSource {
            if CFBitVectorGetBitAtIndex(dataSource.expandedRows, indexPath.row) == 1{

                let bodyHeightDiff = dataSource.expandedHeightDiffForRow(indexPath.row)
                return contractedCellHeight + bodyHeightDiff
            }
        }
        return contractedCellHeight
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //tell the data source to toggle the cell
        dataSource?.toggleRow(indexPath.row)

        //reload the selelcted cell
        tableView.beginUpdates()
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        tableView.endUpdates()
    }

    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        //tell the cell to dump it's media
        let cell = cell as! MedCell
        cell.dump()
    }
}

