//
//  MedCell.swift
//  MedCircle
//
//  Created by Haskel Ash on 8/1/16.
//  Copyright © 2016 Haskel Ash. All rights reserved.
//

import UIKit

class MedCell: UITableViewCell {

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var nameTitleDateLabel: UILabel!
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var mediaImageView: UIImageView!
    @IBOutlet var likesLabel: UILabel!

    func inject(dict dict: NSDictionary, expanded: Bool, heightToExpand: CGFloat) {
        //called from cellForRowAtIndexPath

        if let icon = dict["author"]?["icon"] as? UIImage {
            iconImageView.image = icon
        }

        if let name = dict["author"]?["name"] as? String,
            let title = dict["title"] as? String,
            let dateString = dict["published_at"] as? String,
            let timeAgo = timeAgoSinceString(dateString, numericDates: true) {

            format(name: name, title: title, timeAgo: timeAgo)
        }

        if !expanded {
            if let summary = dict["summary"] as? String {
                summaryLabel.text = summary
                summaryLabel.numberOfLines = 4
            }
        } else {
            if let body = dict["body"] as? String {
                summaryLabel.text = body
                summaryLabel.numberOfLines = 0
            }
        }

        //add height to the summary label if it's expanded
        for (_, constraint) in summaryLabel.constraints.enumerate() {
            if constraint.firstItem as? UIView == summaryLabel
                && constraint.firstAttribute == .Height {
                constraint.constant = 80 + (expanded ? heightToExpand : 0)
            }
        }

        if let media = dict["media"] as? UIImage {
            mediaImageView.image = media
        }

        if let likes = dict["likes_count"] as? Int {
            if likes <= 0 {
                likesLabel.text = nil
            } else if likes == 1 {
                likesLabel.text = "\(likes) person likes this"
            } else {
                likesLabel.text = "\(likes) people like this"
            }
        }
    }

    private func format(name name: String, title: String, timeAgo: String) {
        //formats attributed label for name, title, and date

        //set up bold, regular, and gray attributes
        let boldAttr = [NSFontAttributeName: UIFont.boldSystemFontOfSize(14)]
        let regAttr = [NSFontAttributeName: UIFont.systemFontOfSize(14)]
        let grayAttr = [NSFontAttributeName: UIFont.systemFontOfSize(14), NSForegroundColorAttributeName: UIColor.grayColor()]

        //format is name in bold, "posted" in regular, title in quotes in bold, newline in regular, time ago in gray
        let attrString = NSMutableAttributedString()
        attrString.appendAttributedString(NSAttributedString(string: name, attributes: boldAttr))
        attrString.appendAttributedString(NSAttributedString(string: " posted ", attributes: regAttr))
        attrString.appendAttributedString(NSAttributedString(string: "\"\(title)\"", attributes: boldAttr))
        attrString.appendAttributedString(NSAttributedString(string: "\n", attributes: regAttr))
        attrString.appendAttributedString(NSAttributedString(string: timeAgo, attributes: grayAttr))

        //set label's attributed string
        nameTitleDateLabel.attributedText = attrString
    }

    func dump() {
        //called when cell goes offscreen

        iconImageView.image = nil
        nameTitleDateLabel.attributedText = nil
        summaryLabel.text = nil
        mediaImageView.image = nil
    }
}